import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.LinkedList;


public class ArisanTest {
    ArisanApp arisanApp;

    @BeforeEach
    void setArisan(){
        arisanApp = new ArisanApp();
    }

    @Test
    @DisplayName("cek pemenang ")
    void test1(){
       LinkedList<String> a = new LinkedList<>();
        a.add("A");
        a.add("B");
        a.add("C");
        LinkedList<String> pemenang = ArisanApp.showpemenang(a);
        Assertions.assertEquals(pemenang,ArisanApp.showpemenang(a));
    }

    @Test
    @DisplayName("cek not null")
    void test2(){
        LinkedList<String> a = new LinkedList<>();
        a.add("A");
        a.add("B");
        a.add("C");
        LinkedList<String> pemenang = ArisanApp.showpemenang(a);
        Assertions.assertNotNull(pemenang.get(1),"aman");
    }

    @Test
    @DisplayName("cek index out off bound")
    void test3(){
        LinkedList<String> a = new LinkedList<>();
        a.add("A");
        a.add("B");
        a.add("C");
        LinkedList<String> pemenang = ArisanApp.showpemenang(a);
        pemenang.remove(1);
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> a.get(2),pemenang.get(1));
    }

}
