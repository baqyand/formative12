import java.util.*;

public class ArisanApp {

    static int totPeserta = 15;
    static String[] keluarga = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O"};
    static int duid = 150000;
    static LinkedList<String> orang = new LinkedList<>();
    static LinkedList<String> pemenang = new LinkedList<>();

    public static LinkedList<String> showpemenang(LinkedList<String> orang) {


        for (int j = 0; j < totPeserta; j++) {
            Collections.shuffle(orang);
            int win = 0;
            if (orang.size() == 0) {
                break;
            } else {


                Random winner = new Random();
//            for (String hadir : keluarga){
//                System.out.println(hadir + " Membayar : " + duid);
//            }
                System.out.println("\nSisa Pemenang : " + orang.size());
                for (String a : orang) {
                    win++;
                    System.out.println(a + " kode : " + win);
                }
                int getWin = winner.nextInt(orang.size());
                int whosWin = getWin + 1;
                System.out.println("\n" + whosWin + " adalah angka yang keluar, pemenangnya " + orang.get(getWin));
                System.out.println("Uang yang di dapat : " + duid * totPeserta + "\n");
                pemenang.add(orang.get(getWin));
                orang.remove(getWin);
            }
        }
        return pemenang;
    }

    public static void main(String[] args) {
        orang.addAll(Arrays.asList(keluarga).subList(0, totPeserta));
        showpemenang(orang);
    }
}
